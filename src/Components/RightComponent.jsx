import React from 'react'

function RightComponent() {
  return (
    <div className="h-screen bg-bgImg" >
          <div>
          <h5 className=' text-white font-bold pt-96 text-2xl text-center'>I like laying down on the sand and looking at the moon</h5>
        </div>

        <div className="avatar-group -space-x-6 flex justify-center pt-28">
          <div className="avatar">
            <div className="w-12">
            <img src={require('../Images/raamin.jpg')} />
            </div>
          </div>
          <div className="avatar">
            <div className="w-12">
            <img src={require('../Images/christina.jpg')} />
            {/* nonamesontheway.jpg */}
            </div>
          </div>
          <div className="avatar">
            <div className="w-12">
            <img src={require('../Images/nonamesontheway.jpg')} />
            </div>
          </div>
          <div className="avatar">
            <div className="w-12">
            <img src={require('../Images/lachlan.jpg')} />
            </div>
          </div>
        </div>

  

      <p className=''></p>

    </div>
  )
}

export default RightComponent