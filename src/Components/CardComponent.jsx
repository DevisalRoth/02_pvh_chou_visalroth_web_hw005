import React, { useState } from 'react'
import FormInputComponent from './FormInputComponent'

const CardComponent = ({ info, setInfo }) => {
    const [readMore, setReadMore] = useState([])
    const [newDialog, setNewDialog] = useState({})
    
    const changeStatus = (e) => {
        setInfo((Event)=>
            Event.map((item)=>{
             if(item.id === e){
                if(item.status === "beach"){
                    return {...item, status:"mountain"};
                }
                else if(item.status === "mountain"){
                    return{...item , status:"forest"};
                }else{
                    return {...item , status:"beach"};
                }
             }
            return item;
            }

            ))
       
    }
    return (
        <div className='mt-24 '>
            <p className='text-4xl  font-bold'>Good evening Team!</p>
            <FormInputComponent data={info} setData={setInfo} />

            <div className="grid grid-cols-3">
                {info.map((item) => {
                    return (
                        <div>
                            <div class=" bg-[#05365d] w-72 h-auto p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 mt-24">
                                {/* <p>{item.id}</p> */}
                                <a href="#">
                                    <h5 class="mb-2 text-2xl font-bold tracking-tight text-white dark:text-white">
                                        {item.title}
                                    </h5>
                                </a>

                                <p class="max-w font-normal line-clamp-4 text-white ">
                                    {item.description}
                                </p>

                                <p className='text-white mt-4  font-bold'>People Going:</p>
                                <p className='text-white font-bold text-lg'>{item.peopleGoing}</p>
                             

                                <button  onClick={()=> changeStatus(item.id)} class={
                                    item.status==="beach"?
                                "inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                                :item.status==="mountain"
                                ?"inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-yellow-300 rounded-lg "
                                :"inline-flex items-center px-3 py-2 text-sm font-medium text-center text-white bg-red-700 rounded-lg "
                                
                                }>
                                    {item.status}
                                </button>


                                {/* Buttom read more */}
                                {/* The button to open modal */}
                                <label onClick={() => setReadMore(item)} htmlFor="my-modal" className="btn ml-7">Read more</label>

                                {/* Put this part before </body> tag */}
                                <input type="checkbox" id="my-modal" className="modal-toggle " />
                                <div className="modal">
                                    <div className="modal-box">
                                        <h2 className=' text-lg font-bold'> {readMore.title}</h2>
                                        <h3 className="font-bold text-lg  text-black"></h3>
                                        <p className="py-4 text-black">{readMore.description}</p>
                                        <span>Around  <span className=' font-bold'>{readMore.peopleGoing} </span> people going there</span>
                                        <div className="modal-action">
                                            <label htmlFor="my-modal" className="btn">Exit</label>
                                        </div>
                                    </div>
                                </div>
                                {/* End Buttom read more */}


                            </div>


                        </div>
                    )
                })}
            </div>

        </div>

    )
}

export default CardComponent