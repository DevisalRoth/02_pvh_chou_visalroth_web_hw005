import React from 'react'
import 'flowbite'
function SlideBarComponent() {

    return (
        <div>

            <aside id="default-sidebar" class="fixed top-0 left-0 z-40 w-24 h-screen transition-transform -translate-x-full sm:translate-x-0" aria-label="Sidebar">
                <div class="h-full px-6 py-7 overflow-y-auto bg-gray-50 dark:bg-gray-800">
                    <ul class="space-y-2">
                        <li class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <img src={require('../Images/category_icon.png')} />
                        </li>
                    </ul>

                    <ul className="mt-7 space-y-3.5 ">
                        <li class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <img src={require('../Images/cube.png')} />
                        </li>
                        <li class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700" >
                            <img src={require('../Images/list.png')} />
                        </li>
                        <li class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <img src={require('../Images/messenger.png')} />
                        </li>
                        <li class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <img src={require('../Images/list.png')} />
                        </li>
                        <li class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">

                        </li>
                    </ul>

                    <ul className=" mt-3 space-y-3.5 ">
                        <li class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <img src={require('../Images/success.png')} />
                        </li>

                        <li class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <img src={require('../Images/security.png')} />
                        </li>

                        <li class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <img src={require('../Images/users.png')} />
                        </li>

                    </ul>

                    <ul className=" mt-8 space-y-3.5">
                        <li class="flex items-center p-2 text-base font-normal text-gray-900">
                            <img src={require('../Images/lachlan.jpg')} className=" w-10 h-10 rounded-[100%] "/>
                        </li>
                        <li class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <img src={require('../Images/raamin.jpg')} className=" w-10 h-10 rounded-[100%] "/>
                        </li>
                        <li class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <img src={require('../Images/nonamesontheway.jpg')} className=" w-10 h-10 rounded-[100%] "/>
                        </li>
                        <li class="flex items-center p-2 text-base font-normal text-gray-900 rounded-lg dark:text-white hover:bg-gray-100 dark:hover:bg-gray-700">
                            <img src={require('../Images/plus.png')}/>
                        </li>
                    </ul>
                </div>
            </aside>





        </div>
    )
}

export default SlideBarComponent